import React from 'react'
import c from 'classnames'
import '../assets/immers.css'
import immersIcon from '../assets/immers_logo.png'
import spaceLogo from '../../static/space-logo.png'

export default function Layout (props) {
  const data = window._serverData || {}
  const title = props.title || data.name
  const attributionUrl = props.attributionUrl || data.imageAttributionUrl
  const attribution = props.attribution || data.imageAttributionText
  return (
    <div>
      <div className='aesthetic-effect-crt bg' />
      <div className='content'>
      
        <div className='main-content'>
          <img src={spaceLogo} className='im-logo' alt="" /> 
          <div>
            <hr />
            {props.children}
          </div>
        </div>
        {/* balances layout with title */}
        <h1>&nbsp;</h1>
      </div>

      <div className={c('attribution', { 'with-taskbar': props.taskbar })}>
        {attribution &&
          <a className='aesthetic-green-color' href={attributionUrl}>
            Background: {attribution}
          </a>}
      </div>
      {props.taskbar && (
        <div className='aesthetic-windows-95-taskbar'>
          <div className='aesthetic-windows-95-taskbar-start'>
            <a href={`//${data.hub}`}>
              <img src={data.icon ?? immersIcon} className='immers-icon' /> Enter {data.name}
            </a>
          </div>
          {props.taskbarButtons?.length && (
            <div className='aesthetic-windows-95-taskbar-services'>
              {props.taskbarButtons}
            </div>
          )}
        </div>
      )}
    </div>
  )
}
